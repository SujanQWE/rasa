from typing import Any, Dict, List, Text
from random import shuffle
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.events import Restarted
from rasa_sdk.executor import CollectingDispatcher

import sqlite3


class ActionHelloWorld(Action):
    def name(self) -> Text:
        return "action_hello_world"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        name = tracker.get_slot("name")
        address = tracker.get_slot("address")
        number = tracker.get_slot("number")

        # Connect to the SQLite database
        conn = sqlite3.connect("your_database.db")
        cursor = conn.cursor()

        # Create a table if it doesn't exist
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS user_info (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT,
                address TEXT,
                number TEXT
            )
        """
        )

        # Insert the slot values into the table
        cursor.execute(
            """
            INSERT INTO user_info (name, address, number)
            VALUES (?, ?, ?)
        """,
            (name, address, number),
        )

        # Commit the changes and close the connection
        conn.commit()
        conn.close()

        # Construct the response message
        message = (
            "Your name is: "
            + name
            + ", your address is: "
            + address
            + ", your number is: "
            + number
            + ""
        )

        dispatcher.utter_message(text=message)

        return []


class MyFormAction(Action):
    def name(self) -> Text:
        return "my_form_action"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        intent = tracker.latest_message["intent"].get("name")
        print(intent)
        if intent == "JavaDev":
            dispatcher.utter_message(response="utter_android_developer")
        elif intent == "AngularDev":
            dispatcher.utter_message(response="utter_accountant")
        else:
            dispatcher.utter_message(response="invalid payload")

        return [Restarted()]


subjects = {
    "java": [
        (
            "What is Java?",
            [
                "Java is a high-level programming language",
                "Java is a database management system",
                "Java is a web browser",
                "Java is a markup language",
            ],
            0,
        ),
        (
            "Why is Java popular?",
            [
                "Java is popular because of its platform independence",
                "Java is popular for graphic design",
                "Java is popular for its simplicity",
                "Java is popular for its speed",
            ],
            0,
        ),
        (
            "What is polymorphism?",
            [
                "Polymorphism is a programming language",
                "Polymorphism is a design pattern",
                "Polymorphism is the ability of an object to take on many forms",
                "Polymorphism is a database management system",
            ],
            2,
        ),
        (
            "What is an exception?",
            [
                "An exception is a programming language",
                "An exception is an error that occurs during the execution of a program",
                "An exception is a database management system",
                "An exception is a design pattern",
            ],
            1,
        ),
        (
            "What is an interface?",
            [
                "An interface is a programming language",
                "An interface is a user interface element",
                "An interface is a contract for a class to follow",
                "An interface is a database management system",
            ],
            2,
        ),
        (
            "What is multithreading?",
            [
                "Multithreading is a programming language",
                "Multithreading is a technique to execute multiple threads concurrently",
                "Multithreading is a database management system",
                "Multithreading is a design pattern",
            ],
            1,
        ),
        (
            "What is the Java Virtual Machine (JVM)?",
            [
                "The JVM is a programming language",
                "The JVM is a web server",
                "The JVM is a database management system",
                "The JVM is a virtual machine that executes Java bytecode",
            ],
            3,
        ),
    ],
    "dotnet": [
        (
            "What is Dotnet?",
            [
                "Dotnet is a programming language",
                "Dotnet is a platform",
                "Dotnet is a database",
                "Dotnet is a machine",
            ],
            1,
        ),
        (
            "Why use Dotnet?",
            [
                "We use Dotnet to create games",
                "We use Dotnet to build web applications",
                "We use Dotnet for data analysis",
                "We use Dotnet for graphic design",
            ],
            1,
        ),
        (
            "What is C#?",
            [
                "C# is a programming language",
                "C# is a framework",
                "C# is an operating system",
                "C# is a database",
            ],
            0,
        ),
        (
            "What is ASP.NET?",
            [
                "ASP.NET is a programming language",
                "ASP.NET is a platform",
                "ASP.NET is a web framework",
                "ASP.NET is an application server",
            ],
            2,
        ),
        (
            "What is Entity Framework?",
            [
                "Entity Framework is a database",
                "Entity Framework is a programming language",
                "Entity Framework is an object-relational mapping framework",
                "Entity Framework is a web server",
            ],
            2,
        ),
        (
            "What is CLR?",
            [
                "CLR is a programming language",
                "CLR is a virtual machine",
                "CLR is a database management system",
                "CLR is a framework",
            ],
            1,
        ),
        (
            "What is GAC?",
            [
                "GAC is a database",
                "GAC is a programming language",
                "GAC is a global assembly cache",
                "GAC is an application server",
            ],
            2,
        ),
    ],
    "angular": [
        (
            "What is Angular?",
            [
                "Angular is a programming language",
                "Angular is a framework",
                "Angular is a database",
                "Angular is a machine",
            ],
            1,
        ),
        (
            "Why use Angular?",
            [
                "We use Angular to build web applications",
                "We use Angular for data analysis",
                "We use Angular for graphic design",
                "We use Angular to create mobile applications",
            ],
            0,
        ),
        (
            "What is TypeScript?",
            [
                "TypeScript is a programming language",
                "TypeScript is a framework",
                "TypeScript is an operating system",
                "TypeScript is a database",
            ],
            0,
        ),
        (
            "What is Angular CLI?",
            [
                "Angular CLI is a programming language",
                "Angular CLI is a platform",
                "Angular CLI is a web framework",
                "Angular CLI is a command-line interface",
            ],
            3,
        ),
        (
            "What is Component?",
            [
                "Component is a programming language",
                "Component is a user interface element",
                "Component is a contract for a class to follow",
                "Component is a database management system",
            ],
            1,
        ),
        (
            "What is RxJS?",
            [
                "RxJS is a database",
                "RxJS is a programming language",
                "RxJS is a reactive programming library",
                "RxJS is a web server",
            ],
            2,
        ),
        (
            "What is ng-template?",
            [
                "ng-template is a database",
                "ng-template is a programming language",
                "ng-template is a web template",
                "ng-template is an application server",
            ],
            2,
        ),
    ],
    "flutter": [
        (
            "What is flutter?",
            [
                "Flutter is a programming language",
                "Flutter is a framework",
                "Flutter is a database",
                "Flutter is a machine",
            ],
            1,
        ),
        (
            "Why use flutter?",
            [
                "We use flutter to build cross-platform mobile applications",
                "We use flutter for data analysis",
                "We use flutter for graphic design",
                "We use flutter to create web applications",
            ],
            0,
        ),
        (
            "What is a widget?",
            [
                "A widget is a programming language",
                "A widget is a framework",
                "A widget is a markup language",
                "A widget is a user interface element",
            ],
            3,
        ),
        (
            "What is hot reload?",
            [
                "Hot reload is a programming language",
                "Hot reload is a technique to see changes in code instantly without restarting the app",
                "Hot reload is a database management system",
                "Hot reload is a framework",
            ],
            1,
        ),
        (
            "What is a stateful widget?",
            [
                "A stateful widget is a programming language",
                "A stateful widget is a virtual machine",
                "A stateful widget is a web server",
                "A stateful widget is a widget that can change its state during runtime",
            ],
            3,
        ),
        (
            "What is a pubspec.yaml file?",
            [
                "A pubspec.yaml file is a programming language",
                "A pubspec.yaml file is a configuration file in flutter",
                "A pubspec.yaml file is a database",
                "A pubspec.yaml file is a framework",
            ],
            1,
        ),
        (
            "What is a MaterialApp?",
            [
                "A MaterialApp is a programming language",
                "A MaterialApp is a widget",
                "A MaterialApp is a database",
                "A MaterialApp is a framework",
            ],
            1,
        ),
    ],
}


class ActionGenerateQuestion(Action):
    def name(self) -> Text:
        return "action_generate_question"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        subject = tracker.latest_message["text"]
        subject = subject.lower()

        if subject in subjects:
            chosen_subject = subjects[subject]
            shuffle(chosen_subject)

            questions = chosen_subject
            current_question_index = 0
            score = 0

            question, options, _ = chosen_subject[0]

            message = f"Question 1:\n{question}\n"
            for i, option in enumerate(options):
                message += f"{i + 1}. {option}\n"

            dispatcher.utter_message(text=message)

            return [
                SlotSet("questions", questions),
                SlotSet("current_question_index", current_question_index),
                SlotSet("score", score),
            ]
        else:
            dispatcher.utter_message(
                text=f"No questions found for the subject: {subject}"
            )

        return []


class ActionHandleAnswer(Action):
    def name(self) -> Text:
        return "action_handle_answer"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        answer = int(tracker.latest_message["text"])
        current_question_index = tracker.get_slot("current_question_index")
        score = tracker.get_slot("score")
        questions = tracker.get_slot("questions")

        if (
            questions
            and current_question_index is not None
            and 0 <= current_question_index < len(questions)
        ):
            _, _, correct_answer = questions[current_question_index]

            if answer == correct_answer + 1:
                score += 1
                dispatcher.utter_message(text="Correct answer!")
            else:
                dispatcher.utter_message(text="Incorrect answer.")

            current_question_index += 1

            if current_question_index < len(questions):
                question, options, _ = questions[current_question_index]

                message = f"Question {current_question_index + 1}:\n{question}\n"
                for i, option in enumerate(options):
                    message += f"{i + 1}. {option}\n"

                dispatcher.utter_message(text=message)
            else:
                dispatcher.utter_message(text=f"You scored: {score}/{len(questions)}")

            return (
                SlotSet("current_question_index", current_question_index),
                SlotSet("score", score),
            )
        else:
            dispatcher.utter_message(text="No questions found.")

        return []


class ActionResetForm(Action):
    def name(self) -> Text:
        return "action_reset_form"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        slots_to_reset = ["questions", "current_question_index", "score"]
        for slot in slots_to_reset:
            tracker.slots.pop(slot, None)

        return []
